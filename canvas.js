


let canvas = document.getElementById('canvas');


let longueur_width = 1000;
let longueur_height = 650;


canvas.width = longueur_width;
canvas.height = longueur_height;


let c = canvas.getContext('2d');


let rayon = 4;
let nb_cercles = 400;

function rand_signe() {
    const n = Math.random();
    if (n > 0.5) {
        return 1;
    }
    else {
        return -1;
    }
}

function rand(min, max) {
    return min + Math.random() * (max - min);
}

function centre() {
    return {
        couleur: 'blue',
        x: rand(rayon, longueur_width - rayon),
        y: rand(rayon, longueur_height - rayon),
        dx: (rand_signe() * rand(5,10))/4,
        dy: (rand_signe() * rand(5,10))/4,
        
    };
}


function dessine(o) {
    c.beginPath();
    c.arc(o.x, o.y, rayon, 0, Math.PI*2, false);
    c.fillStyle = o.couleur;
    c.fill();
}




function collision(o1, o2){
    let dx = o2.x - o1.x;
    let dy = o2.y - o1.y;
    let dist = Math.pow((Math.pow(dx, 2) + Math.pow(dy, 2)), 0.5);
    if (dist <= 2.1*rayon && dist != 0){
      o1.dx = -o1.dx;
      o2.dx = -o2.dx;
        if ((o1.couleur == 'red' && o2.couleur == 'blue') || (o2.couleur == 'red' && o1.couleur == 'blue')){
            o1.couleur = 'red'; o2.couleur = 'red';
        }
    }
}



function maj(o) {
    if (o.x + rayon >= longueur_width || o.x - rayon < 0) {
        o.dx = -o.dx;
    }
    if (o.y + rayon >= longueur_height || o.y - rayon < 0) {
        o.dy = -o.dy;
    }
    o.x += o.dx;
    o.y += o.dy;
    dessine(o);
}

let cercles = [];

for (let i = 0; i < nb_cercles; i++) {
    cercles.push(centre());
}



cercles[0].couleur = 'red';



function bouge() {
    requestAnimationFrame(bouge);
    c.clearRect(0, 0, longueur_width, longueur_height);
    for (let o=0; o <   nb_cercles; o++) {
      for(let k = 0; k < nb_cercles; k++){
        collision(cercles[o], cercles[k]);
      }
        maj(cercles[o]);
    }
}
 // for (let i = 0; i < nb_cercles; i++) {
 //        // for (let j = i; j < nb_cercles; j++) {
 //            choc(cercles[i], cercles[j]);
 //        }
 //        maj(cercles[i]);
 //    }

// bouge();





document.getElementById("boutton_sans_confine").onclick = function() {
  nb_cercles = 90;
  bouge();
}



document.getElementById("boutton_avec_confine").onclick = function() {
  nb_cerclesTT = 10;
  bouge();
}

function effacer(){
  nb_cercles = 0;
}


//=============================ancien canvas=================================









//     window.onload = function() {
//       var canvas = document.createElement("canvas"),
//         contexte = canvas.getContext("2d");
//         TableauParticulesRougesEtBlanches = [];
//         TableauCollisionRouge = [];
//         // TableauParticulesBlanches = [];
//         particuleIndex = 0,
//         // particuleIndexRouges = 0, 
//         UneParticuleRougeNum = 5,
//         UneParticuleBlancheNum = 25;

//       canvas.width = 400
//       canvas.height = 400

//       document.body.appendChild(canvas);
      
//     contexte.fillStyle = "black";
//     contexte.fillRect(0, 0, canvas.width, canvas.height);
//     //collision(UneParticuleBlanche(), UneParticuleRouge());
 
// //==============================================================================
// // Les rouges
// // Début boucle for de création des particules rouges
//  // alert('Compteur une particule rouge : ' + UneParticuleRougeNum);
//  for (var i = 0; i < UneParticuleRougeNum; i++){
//    new UneParticuleRouge();
//    // alert('UneParticuleRouge new : ' + i);
//  }
//  window.setInterval(function(){
//    contexte.fillStyle = "black";
//    contexte.fillRect(0, 0, canvas.width, canvas.height);
      
//    for (var j in TableauParticulesRougesEtBlanches){//================
//     // alert('j = ' + j);
//     TableauParticulesRougesEtBlanches[j].draw();
//        }
//    }, 10);

//  UneParticuleRouge.prototype.draw = function(){
//   this.x += this.vx;
//   this.y += this.vy;

//   if (Math.random() < 0.1){
//     this.vx = Math.random() * 10-5;
//   }
//   this.life++;
//   if(this.life >= this.maxLife){
//     delete TableauParticulesRougesEtBlanches[this.id];//=====================
//   }


//   //  rebondir sur les côtés
//   if(this.y + this.vy < 0) {  //coin supérieur gauche
//     this.vy = -this.vy;
//   }
//   if(this.y + this.vy > canvas.height) {
//     this.vy = -this.vy;
//   }
//   if(this.x + this.vx > canvas.width || this.x + this.vx < 0) {
//     this.vx = -this.vx;
//   }
//   if(this.y + this.vy > canvas.height || this.y + this.vy < 0) {
//     this.vy = -this.vy;
//   }
//   contexte.fillStyle = this.color;
//   contexte.fillRect(this.x, this.y, 10, 10);
//  };

// //==============================================================================
// // Les blancs
// // alert('Compteur une particule blanche : ' + UneParticuleBlancheNum);
//  for (var i = 0; i < UneParticuleBlancheNum; i++){
//    new UneParticuleBlanche();
// //   alert('UneParticuleBlanche new : ' + i);
//  }

//  // window.setInterval(function(){
//   // contexte.fillStyle = "black";
//   // contexte.fillRect(0, 0, canvas.width, canvas.height);
//   // for (var i in TableauParticulesBlanches){
//    // TableauParticulesBlanches[i].draw();
//       // }
//    // }, 10);

// // Début UneParticuleBlanche
//  UneParticuleBlanche.prototype.draw = function(){
//   this.x += this.vx;
//   this.y += this.vy;
// // alert("je suis dans fonction draw des blanches");
//   if (Math.random() < 0.1){
//     this.vx = Math.random() * 10-5;
//   }
//   this.life++;
//   if(this.life >= this.maxLife){
//     delete TableauTableauParticulesBlanches[this.id];
//   }
//   if(this.y + this.vy < 0) {
//     this.vy = -this.vy;
//   }
//   if(this.y + this.vy > canvas.height) {
//     this.vy = -this.vy;
//   }
//   if(this.x + this.vx > canvas.width || this.x + this.vx < 0) {
//     this.vx = -this.vx;
//   }
//   if(this.y + this.vy > canvas.height || this.y + this.vy < 0) {
//     this.vy = -this.vy;
//   }
// // ajout Christophe
//   contexte.fillStyle = this.color;
//   contexte.fillRect(this.x, this.y, 10, 10);
//  };

// //==============================================================================
// //Traitement collision entre les blancs et les rouges
// // alert('Début Procédure de collision entre les particules');

// //window.setInterval(function(){
// //  DetectionCollision()}, 15);

// // Fin Déterminer les collisions entre les paerticules rouges et les particules blanches

// // alert('Fin Procédure de collision entre les particules');


// // alert("debut fonction");



//  this.UneParticuleRougeInside = function ( UneParticuleBlanche ){
//   // alert(this.x);
//   // alert(UneParticuleBlanche.width);

//   if (this.x < UneParticuleBlanche.x + UneParticuleBlanche.width &&
//     this.x + this.width  > UneParticuleBlanche.x &&
//     this.y < UneParticuleBlanche.y + UneParticuleBlanche.height &&
//     this.y + this.height > UneParticuleBlanche.y)
//   {
//     alert("collision");
//   }
// }


// //==============================================================================
// //Definition des fonctions


// // Fonction UneParticuleRouge
// function UneParticuleRouge(){
//   this.x = Math.random() *10 - 5;
//   this.y = Math.random() *10 - 5;
//   this.vx = Math.random() *10 - 5;
//   this.vy = Math.random() * 10 - 5;
//   this.id = particuleIndex;
//   this.life = 0;
//   this.maxLife = Math.random() * 100000 + 30000;
//   this.color = "red";
//   this.width = 50;
//   this.height = 50;
//   // alert ('particuleIndexRouges : ' + particuleIndexRouges);
//   TableauParticulesRougesEtBlanches[particuleIndex] = this;//===particuleIndexRouges
// //  TableauCollisionRouge = new Array(this.x, this.y, this.vx, this.vy);
//   TableauCollisionRouge.push(this.id, this.x, this.y, this.width, this.height);
//   // alert('TableauCollisionRouge 0 ' + TableauCollisionRouge[0]);
//   // alert('TableauCollisionRouge 1 ' + TableauCollisionRouge[1]);
//   // alert('TableauCollisionRouge 2 ' + TableauCollisionRouge[2]);
//   // alert('TableauCollisionRouge 3 ' + TableauCollisionRouge[3]);
//   // alert('TableauCollisionRouge 4 ' + TableauCollisionRouge[4]);
//   particuleIndex++;//===========================
//   return TableauCollisionRouge;
//  }

//  // Fonction UneParticuleBlanche
// function UneParticuleBlanche(){
//   // alert("debut");
//   this.x = canvas.width / 2;
//   this.y = canvas.height / 2;
//   this.vx = Math.random() *10 - 5;
//   this.vy = Math.random() * 10 - 5;
//   this.id = particuleIndex; //==============particuleIndexBlanches
//   this.life = 0;
//   this.maxLife = Math.random() * 100000 + 30000;
//   this.color = "white";
//   this.width = 50;
//   this.height = 50;
//   TableauParticulesRougesEtBlanches[particuleIndex] = this;//===particuleIndexBlanches
//   particuleIndex++;//====================particuleIndexBlanches++
//   var tableau2 = new Array(this.x, this.y, this.width, this.height);
//   // alert("fin");
//   // alert(tableau2[0]);
//   // alert(tableau2[1]);
//   // alert(tableau2[2]);
//   // alert(tableau2[3]);
//   return tableau2;
// }

// // alert(TableauParticulesRougesEtBlanches);



// function DetectionCollision(){
// // alert("debut recherche");
// //alert('Début fonction DetectionCollision');
//  // alert("UneParticulesRouge()[0]");
// if (UneParticuleBlanche()[0] < (UneParticuleRouge()[0] + UneParticuleRouge()[2])){
//       // alert(UneParticuleBlanche()[0]);// this.color = "green";
//     }
// if (UneParticuleBlanche()[0] + UneParticuleBlanche()[2] > UneParticuleRouge()[0]){
//       // alert("collison");// this.color = "green";
//     }
// if(UneParticuleBlanche()[1] < UneParticuleRouge()[1] + UneParticuleRouge()[3]){
//       // alert("collison");// this.color = "green";
//      }
// if (UneParticuleBlanche()[3] + UneParticuleBlanche()[1] > UneParticuleRouge()[1]){
//       // alert("collision");// this.color = "green";
//    }


// function Collision(o1, o2){
//   let dx = o2.x - o1.x
//   let dy = o2.y - o1.y
//   if(dx <= 50 && dy <= 50){
//     alert("collision");
//   }
// }


// // collision(UneParticuleBlanche, UneParticuleRouge);





// //alert('Fin fonction DetectionCollision');

// }

//   setInterval(function(){
//    DetectionCollision()
//   }, 10);


// contexte.fillStyle = this.color;
// contexte.fillRect(this.x, this.y, 10, 10);


// };// fin window.onload = function()
